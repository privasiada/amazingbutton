﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmazingButton
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button me = sender as Button;
            Random randonGen = new Random();
            
            me.BackColor = Color.FromArgb(randonGen.Next(255), randonGen.Next(255), randonGen.Next(255));
        }
    }
}
